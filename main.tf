terraform {
  required_providers {
    aws = {
      version = ">= 2.7.0"
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

data "aws_vpc" "vpc" {
  default = true
 
} 

# module "subnet" {
#   source = "./module/subnet"
#   vpc_id = data.aws_vpc.vpc.id
# }

module "bean_stalk_app" {
  source = "./module/bean_stalk_app"
}

module "beanstalk_app_ver" {
  source  = "./module/beanstalk_app_ver"
  application    = module.bean_stalk_app.name
  bucket  = module.s3.id 
  key     = module.s3_object.id

}

module "bean_stalk_env" {
  source = "./module/bean_stalk_env"
  application = module.bean_stalk_app.name
  version_label = module.beanstalk_app_ver.name
  RDS_PASSWORD = module.ssm_parameter.value
  SecurityGroups = module.secgroup.name
  RDS_HOSTNAME = module.rds.RDS_HOSTNAME
  RDS_USERNAME = module.rds.RDS_USERNAME
  RDS_DB_NAME = module.rds.RDS_DB_NAME 
  # vpc_id = data.aws_vpc.vpc.id
  # ELBSubnets = module.subnet.subnet_list[0]
  # subnets = module.subnet.subnet_list[0]
}

module "rds" {
  source = "./module/rds"
  vpc_security_group_ids = module.secgroup.id
  password = module.ssm_parameter.value
  # subnet_list = module.subnet.subnet_list
}

module "s3" {
  source = "./module/s3"
}

module "s3_object" {
  source = "./module/s3_object"
  bucket = module.s3.bucket
}

module "secgroup" {
  source = "./module/secgroup"
  vpc_id = data.aws_vpc.vpc.id
}

module "ssm_parameter" {
  source = "./module/ssm_parameter"
}
