variable "name" {   
  default = "tf-test-name"
}

variable "solution_stack_name" {
    default = "64bit Amazon Linux 2 v3.3.6 running PHP 8.0"
  
}

variable "application" {
    type = string
  
}

variable "RDS_HOSTNAME" {
  type = string
}

variable "iaminstanceprofile" {
  default =  "aws-elasticbeanstalk-ec2-role"

}

variable "RDS_PASSWORD" {
  type = string
  
}

variable "version_label" {
  type = string
  
}

# variable "vpc_id" {
#   type = string
  
# }

variable "SecurityGroups" {
  type = string
  
}

variable "RDS_USERNAME" {
  type = string
  
  
}
variable "RDS_DB_NAME" {
  type = string
 
  
}


# variable "ELBSubnets"{
#   type = string
# }

# variable "subnets"{
#   type = string
# }