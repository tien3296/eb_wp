resource "aws_elastic_beanstalk_environment" "tfenvtest" {
  name                = var.name
  application         = var.application
  solution_stack_name = var.solution_stack_name 
  version_label= var.version_label
  # version_label= aws_elastic_beanstalk_application_version.MyAppVer.name
  
  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = var.iaminstanceprofile
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "RDS_HOSTNAME"
    value     = var.RDS_HOSTNAME
  }
  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "RDS_PASSWORD"
    value     = var.RDS_PASSWORD
  }

  setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "RDS_USERNAME"
    value     = var.RDS_USERNAME
  }
 setting {
    namespace = "aws:elasticbeanstalk:application:environment"
    name      = "RDS_DB_NAME"
    value     = var.RDS_DB_NAME
  }


  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = var.SecurityGroups
  }
  
 

  # setting {
  #   namespace = "aws:ec2:vpc"
  #   name      = "VPCId"
  #   value     = var.vpc_id
  # }

#   setting {
#     namespace = "aws:ec2:vpc"
#     name      = "ELBSubnets"
#     value     = var.ELBSubnets
#   }
#  setting {
#     namespace = "aws:ec2:vpc"
#     name      = "subnets"
#     value     = var.subnets
#   }
}