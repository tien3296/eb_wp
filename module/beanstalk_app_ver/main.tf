resource "aws_elastic_beanstalk_application_version" "MyAppVer" {
  name        = "tf-test-1.0"
  application = var.application
  description = "application version created by terraform"
  bucket      = var.bucket
  key         = var.key
}
