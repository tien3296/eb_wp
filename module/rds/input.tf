variable "identifier" {
    default = "mydb"
  
}

variable "allocated_storage" {
  default = 20
}

variable "engine" {
    default = "mysql"
}

variable "instance_class" {
    default = "db.t2.micro"
  
}

variable "name" {
    default = "wordpress"
  
}

variable "username" {
    default = "admin"
}

variable "password" {
  type = string 
}

variable "vpc_security_group_ids" {
    type = string
  
}
# variable "subnet_list"{
#     type = list(string)
# }