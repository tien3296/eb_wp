resource "aws_db_instance" "MyRDS" {
  identifier= var.identifier
  allocated_storage    = var.allocated_storage
  engine               = var.engine
  instance_class       = var.instance_class
  name                 = var.name
  username             = var.username
  password             = var.password
  vpc_security_group_ids=[var.vpc_security_group_ids,]
  # db_subnet_group_name = aws_db_subnet_group.MyRDSSubnetgroup.name
  skip_final_snapshot  = true
}

# resource "aws_db_subnet_group" "MyRDSSubnetgroup" {
#   name       = "main"
#   subnet_ids = var.subnet_list
#   tags = {
#     Name = "My DB subnet group"
#   }
# }
