output "RDS_HOSTNAME" {
    value = aws_db_instance.MyRDS.address
}

output "RDS_USERNAME" {
  value = aws_db_instance.MyRDS.username
}

#  RDS_DB_NAME: 'wordpress'
output "RDS_DB_NAME" {
  value = aws_db_instance.MyRDS.name
}

