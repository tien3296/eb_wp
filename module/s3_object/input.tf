variable bucket{
  type = string
}

variable "key" {
    default = "beanstalk/go-v1.zip"
}

variable "path"{
    default = "wordpress/artifact.zip"
}
