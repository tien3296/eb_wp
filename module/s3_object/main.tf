resource "aws_s3_bucket_object" "Mysource" {
  bucket = var.bucket
  key    = var.key
  source = var.path
}
