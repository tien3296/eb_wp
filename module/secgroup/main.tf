resource "aws_security_group" "RDS" {
  name        = "RDS"
  description = "Allow TLS inbound traffic"
  vpc_id = var.vpc_id

  ingress = [
    {
      description      = "self-RDS"
      from_port        = 3306
      to_port          = 3306
      protocol         = "tcp"
      cidr_blocks 	   = "0.0.0.0/0"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      security_groups  =[]
      prefix_list_ids  =[]
      self             = true
      
    }
    
  ]
   egress = [
    {

      description      = "self-RDS"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      cidr_blocks 	   = "0.0.0.0/0"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      security_groups  = []
      prefix_list_ids  =[]
      self = true
    }
  ]

  tags = {
    Name = "RDS"
  }
}

