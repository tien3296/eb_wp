resource "aws_ssm_parameter" "secret" {
  name        = "/production/database/password/master"
  description = "The parameter description"
  type        = "SecureString"
  value       = random_password.pass_RDS.result

  tags = {
    environment = "production"
  }
}

resource "random_password" "pass_RDS" {
  length           = 16
  special          = true
  override_special = "_%@"

}

