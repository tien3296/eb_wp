resource "aws_subnet" "subnet1" {
  vpc_id     = var.vpc_id
  cidr_block = "10.0.1.0/25"
  availability_zone_id = "use1-az1"
  tags = {
    Name = "Main"
  }
}

resource "aws_subnet" "subnet2" {
  vpc_id     = var.vpc_id
  cidr_block = "10.0.2.0/25"
  availability_zone_id = "use1-az2"
  tags = {
    Name = "Main"
  }
}