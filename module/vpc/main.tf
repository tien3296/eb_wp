resource "aws_vpc" "vpc" {
  cidr_block = "10.0.0.0/16"
  
}
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "main"
  }
}